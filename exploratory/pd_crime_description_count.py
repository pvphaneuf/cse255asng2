import csv

import util

from pprint import pprint

from exploratory import common

import json


__author__ = 'Patrick Phaneuf'


class AutoVivification(dict):

    """Implementation of perl's autovivification feature."""

    def __getitem__(self, item):

        try:

            return dict.__getitem__(self, item)

        except KeyError:

            value = self[item] = type(self)()

            return value


def main():

    pd_crime_dict = AutoVivification()

    with open("training_crime.csv", 'rb') as csv_file:

        csv_reader = csv.reader(csv_file, delimiter=',', quotechar='"')

        for entry_list in csv_reader:

            if util.is_header(entry_list):

                continue

            else:

                pd_district = entry_list[common.PD_DISTRICT_INDEX]

                crime_category = entry_list[common.CATEGORY_INDEX]

                crime_description = entry_list[common.DESCRIPTION_INDEX]

                if bool(pd_crime_dict[pd_district][crime_category][crime_description]):

                    pd_crime_dict[pd_district][crime_category][crime_description] += 1

                else:

                    pd_crime_dict[pd_district][crime_category][crime_description] = 1

    # with open("pd_crime.json", 'w') as json_file:
    #
    #     json.dump(pd_crime_dict, json_file)

    for pd_district, crime_category_dict in pd_crime_dict.iteritems():

        crime_category_list = []

        for crime_category, crime_description_dict in crime_category_dict.iteritems():

            crime_description_sum = 0

            for crime_description, count in crime_description_dict.iteritems():

                crime_description_sum += count

            crime_category_count_tuple = (crime_description_sum, crime_category)

            crime_category_list.append(crime_category_count_tuple)

        crime_category_list.sort()

        crime_category_list.reverse()

        print(str(pd_district) + "," + str(crime_category_list))

        pd_district_crime_sum = 0

        for crime_category_count_tuple in crime_category_list:

            pd_district_crime_sum += crime_category_count_tuple[0]

        print(str(pd_district) + "," + str(pd_district_crime_sum))


if __name__ == "__main__":
    main()