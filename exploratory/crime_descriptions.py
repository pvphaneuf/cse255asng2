import csv

import util

from pprint import pprint


__author__ = 'Patrick Phaneuf'

DESCRIPTION_INDEX = 2

CATEGORY_INDEX = 1


def main():

    crime_category_description_dict = {}

    with open("training_crime.csv", 'rb') as csv_file:

        csv_reader = csv.reader(csv_file, delimiter=',', quotechar='"')

        for entry_list in csv_reader:

            if util.is_header(entry_list):

                continue

            else:

                crime_category = entry_list[CATEGORY_INDEX]

                if crime_category in crime_category_description_dict.keys():

                    crime_category_description_dict[crime_category].append(entry_list[DESCRIPTION_INDEX])

                else:

                    crime_category_description_dict[crime_category] = [entry_list[DESCRIPTION_INDEX]]

    output_dict = {}

    for key, value in crime_category_description_dict.iteritems():

        output_dict[key] = set(value)

    pprint(output_dict)


if __name__ == "__main__":
    main()