import csv

import util

import common


__author__ = 'Patrick Phaneuf'

DISTRICT_INDEX = 4


def main():

    pd_district_list = []

    with open("training_crime.csv", 'rb') as csv_file:

        csv_reader = csv.reader(csv_file, delimiter=',', quotechar='"')

        for entry_list in csv_reader:

            if util.is_header(entry_list):

                continue

            else:

                pd_district_list.append(entry_list[common.PD_DISTRICT_INDEX])

    pd_district_set = set(pd_district_list)

    print(pd_district_set)


if __name__ == "__main__":
    main()